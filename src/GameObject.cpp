#include "GameObject.h"
#include <string>
#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600


using namespace std;

double fX, fY;
bool isBack = false;

GameObject::GameObject(){}

void GameObject::moveAutomatic(double time, GameObject *gObject, double factorVel){

    isBack ? fX -= (time / factorVel ) : fX += ( time / factorVel );

    if(gObject->rect.x + gObject->rect.w >= SCREEN_WIDTH) isBack = true;
    else if(gObject->rect.x < 0) isBack = false;

    gObject->rect.x = fX;

}

void moveManual(){



}
void GameObject::loadMedia(SDL_Renderer *gRenderer, char *path){
    texture = SDL_CreateTextureFromSurface(gRenderer, SDL_LoadBMP(path));

}
