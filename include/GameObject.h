#include <SDL.h>

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

using namespace std;

class GameObject
{
    public:
        SDL_Texture *texture;
        SDL_Rect rect;
        SDL_Surface *surface;

        GameObject();
        void loadMedia(SDL_Renderer *gRenderer, char* path);
        void moveAutomatic(double time, GameObject *gObject, double factorVel);
        //void moveManual();
};

#endif // GAMEOBJECT_H
