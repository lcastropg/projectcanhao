#include <SDL.h>
#include <SDL_ttf.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <time.h>
#include "GameObject.h"

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 600
#define SHAPE_SIZE 16

using namespace std;

SDL_Window *gWindow = NULL;
SDL_Renderer *gRenderer = NULL;
SDL_Surface *gScreenSurface = NULL;

GameObject gCanhao;
GameObject gGretchen;
GameObject gBala;
GameObject gConga;

void runGame();
//void moveAutomatic(double time, GameObject gObject, double factorVel);

int qtd_bala =15;
int atirar=0;

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
    LARGE_INTEGER li;
    if(!QueryPerformanceFrequency(&li))
	cout << "QueryPerformanceFrequency failed!\n";

    PCFreq = double(li.QuadPart)/1000.0;

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;
}
double GetCounter()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return double(li.QuadPart-CounterStart)/PCFreq;
}

bool init() {

	if (SDL_Init(SDL_INIT_VIDEO) < 0)  return false;

	gWindow = SDL_CreateWindow("Gretchen on fire!"
		, SDL_WINDOWPOS_UNDEFINED
		, SDL_WINDOWPOS_UNDEFINED
		, SCREEN_WIDTH
		, SCREEN_HEIGHT
		, SDL_WINDOW_SHOWN);

	if (gWindow == NULL) return false;

	if (TTF_Init() < 0) return false;

	gRenderer = SDL_CreateRenderer(gWindow, -1, 0);
	SDL_SetRenderDrawColor(gRenderer, 255, 0, 0, 255);
	gScreenSurface = SDL_GetWindowSurface(gWindow);

	return true;
}
//----------------------------------------------------------
bool loadMedia(){

    char *sCanhao = "canhao.bmp", *sBala = "bala.bmp", *sGretchen = "monster.bmp", *sConga = "conga.bmp";

	gBala.loadMedia(gRenderer,sBala);
	gGretchen.loadMedia(gRenderer,sGretchen);
	gCanhao.loadMedia(gRenderer,sCanhao);
	gConga.loadMedia(gRenderer, sConga);

	return (gCanhao.texture != NULL && gGretchen.texture != NULL && gBala.texture!= NULL );
}
//----------------------------------------------------------
void close() {

	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	SDL_Quit();
}
//----------------------------------------------------------

bool initGameObjects(){

    bool init;

    gCanhao.rect.x = 20;
    gCanhao.rect.y = 385;
    gCanhao.rect.w = 200;
    gCanhao.rect.h = 214;

    gGretchen.rect.x = 10;
    gGretchen.rect.y = 10;
    gGretchen.rect.w = 120;
    gGretchen.rect.h = 143;

    gBala.rect.x = 20 + gCanhao.rect.w/2;
    gBala.rect.y = 385;
    gBala.rect.w = 40;
    gBala.rect.h = 40;

    gConga.rect.w = 40;
    gConga.rect.h = 40;
    gConga.rect.x = gGretchen.rect.x + 40;
    gConga.rect.y = gGretchen.rect.y + 30;

    loadMedia() ? init = true : init = false;
    return init;
}

int main(int argc, char* args[]) {


    if (!init()) {
		puts("Falhou init\n");
		return 0;
	}

	if(initGameObjects()){
        runGame();
        close();
	}
    else{
        cout << "Falhou media!";
        return 1;
    }

	return 0;
}
//lllll


int Colisao(int c)
{
    if((((gBala.rect.x<=gGretchen.rect.x+gGretchen.rect.h) && (gBala.rect.x>gGretchen.rect.x))&&
       ((gBala.rect.y<=gGretchen.rect.y+gGretchen.rect.w)&&(gBala.rect.y>gGretchen.rect.y))))
        c=100;

    if(((gConga.rect.x<=gCanhao.rect.x+gCanhao.rect.h) && (gConga.rect.x>gCanhao.rect.x))&&
       ((gConga.rect.y<=gCanhao.rect.y+gCanhao.rect.w)&&(gConga.rect.y>gCanhao.rect.y)))
       c=15;
// else c= 150;
    //to do more code

return c;
}

void runGame(){

    SDL_Event event;
    bool gameLoop = true, lback = false;
    double fGx, fGy, fCx, fCy, vel, cont, cont2;
    double factor = 1.0;

    int a = 255;
    SDL_RenderCopy(gRenderer, gCanhao.texture , NULL, &gCanhao.rect);
    SDL_RenderCopy(gRenderer, gGretchen.texture, NULL, &gGretchen.rect);
    SDL_RenderCopy(gRenderer, gBala.texture, NULL, &gBala.rect);
    SDL_SetRenderDrawColor(gRenderer, a, 255,255, 255);
    SDL_RenderPresent(gRenderer);

    while(gameLoop){

            StartCounter();
            //lback ? fGx -=vel : fGx += vel;

            gGretchen.moveAutomatic(vel,&gGretchen,factor);


            fGy += vel;
            fCx += vel;
            fCy += vel;

//            gGretchen.rect.x = fGx;
            gConga.rect.x = fCx;
            gConga.rect.y = fCy;

//            if(gGretchen.rect.x + gGretchen.rect.w >= SCREEN_WIDTH) lback = true;
//            else if(gGretchen.rect.x < 0) lback = false;

            if(gConga.rect.y > SCREEN_HEIGHT){
                gConga.rect.y = gGretchen.rect.y + 30;
                gConga.rect.x = gGretchen.rect.x + 40;
                fCy = gConga.rect.y;
                fCx = gConga.rect.x;
            }

            a=Colisao(a);

            if(atirar==1)
            {
                  gBala.rect.y -=1;
                  if(gBala.rect.y==0)
                  {
                      gBala.rect.x =gCanhao.rect.x+gCanhao.rect.w/2;
                      gBala.rect.y = gCanhao.rect.y;
                      atirar=0;
                      a=255;
                  }
            }

            while (SDL_PollEvent(&event)){

                if (event.type == SDL_QUIT)
                    gameLoop = false;
                    else if (event.type == SDL_KEYDOWN){

                        switch (event.key.keysym.sym){
                            case SDLK_RIGHT:
                                gCanhao.rect.x += 10;
                                if(gBala.rect.y==gCanhao.rect.y)
                                gBala.rect.x+= 10;
                                break;
                            case SDLK_LEFT:
                                gCanhao.rect.x -= 10;
                                if(gBala.rect.y==gCanhao.rect.y)
                                gBala.rect.x-= 10;

                                break;
                            case SDLK_SPACE:
                                  gBala.rect.x = gCanhao.rect.x+gCanhao.rect.w/2;
                                  atirar=1;
                                  qtd_bala-=10;
                                break;
                            default:
                                break;
                        }
                }
            }
                SDL_RenderClear(gRenderer);
                SDL_RenderCopy(gRenderer, gCanhao.texture,NULL, &gCanhao.rect);
                SDL_RenderCopy(gRenderer, gGretchen.texture,NULL, &gGretchen.rect);
                SDL_RenderCopy(gRenderer, gBala.texture,NULL, &gBala.rect);
                SDL_RenderCopy(gRenderer, gConga.texture,NULL, &gConga.rect);

                SDL_SetRenderDrawColor(gRenderer, a, 255, 255, 255);
                SDL_RenderPresent(gRenderer);
                vel = GetCounter();


    }
}
